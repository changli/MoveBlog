package action;

import common.AuthenticationTool;
import common.JsonMsg;
import org.apache.commons.lang3.tuple.Pair;
import oschina.BlogApi;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/action/syscatalog")
public class SysCatalogAction extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// 查询授权信息
        Pair<String, String> userToken = AuthenticationTool.ME.getTokenFromCookie(request.getCookies());
        if (userToken == null) {
            JsonMsg.json_out(JsonMsg.jsonError("请先授权!"), response);
            return;
        }

        String user = userToken.getLeft();

        String reString = BlogApi.getBlogSysCatalog(user);
        JsonMsg.json_out(reString, response);
    }
}
